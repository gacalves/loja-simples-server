﻿using System;
using System.ComponentModel.DataAnnotations;

namespace LojaSimplesServer.Model
{
    public class Cliente
    {
        [Key]
        public String Cpf { get; set; }
        [Required]
        public String Email { get; set; }
        [Required]
        public String Senha { get; set; }
        [Required]
        public String Nome { get; set; }
        [Required]
        public String Endereco { get; set; }
        [Required]
        public String Municipio { get; set; }
        [Required]
        public String Estado { get; set; }
        [Required]
        public String Telefone { get; set; }
    }
}
