﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LojaSimplesServer.Model
{
    public class CredenciaisLogin
    {
        public String Email { get; set; }
        public String Senha { get; set; }
    }
}
