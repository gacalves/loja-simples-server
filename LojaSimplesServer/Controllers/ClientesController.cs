﻿using LojaSimplesServer.Model;
using LojaSimplesServer.Repository;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System.Linq;
using System.Threading.Tasks;

namespace LojaSimplesServer.Controllers
{

    [Produces("application/json")]
    [Route("api/Clientes")]
    public class ClientesController : Controller
    {
        private readonly LojaSimplesContext _context;

        public ClientesController(LojaSimplesContext context)
        {
            _context = context;
        }
        
        // GET: api/Clientes/{cpf}
        [HttpGet("{cpf}")]
        public async Task<IActionResult> GetCliente([FromRoute] string cpf)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var cliente = await _context.Cliente.SingleOrDefaultAsync(m => m.Cpf == cpf);

            if (cliente == null)
            {
                return NotFound();
            }

            return Ok(cliente);
        }

        // PUT: api/Clientes/{cpf}
        [HttpPut("{cpf}")]
        public async Task<IActionResult> PutCliente([FromRoute] string cpf, [FromBody] Cliente cliente)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (cpf != cliente.Cpf)
            {
                return BadRequest();
            }

            _context.Entry(cliente).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!ClienteExists(cpf))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/Clientes
        //[AllowAnonymous]
        [HttpPost]
        public async Task<IActionResult> PostCliente([FromBody] Cliente cliente)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            _context.Cliente.Add(cliente);
            await _context.SaveChangesAsync();

            return CreatedAtAction("GetCliente", new { cpf = cliente.Cpf }, cliente);
        }

        // DELETE: api/Clientes/{cpf}
        [HttpDelete("{cpf}")]
        public async Task<IActionResult> DeleteCliente([FromRoute] string cpf)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var cliente = await _context.Cliente.SingleOrDefaultAsync(m => m.Cpf == cpf);
            if (cliente == null)
            {
                return NotFound();
            }

            _context.Cliente.Remove(cliente);
            await _context.SaveChangesAsync();

            return NoContent();
        }

        private bool ClienteExists(string cpf)
        {
            return _context.Cliente.Any(e => e.Cpf == cpf);
        }
    }
}