﻿using System;
using System.Linq;
using System.Threading.Tasks;
using LojaSimplesServer.Model;
using LojaSimplesServer.Repository;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace LojaSimplesServer.Controllers
{
    [Produces("application/json")]
    [Route("api/Login")]
    public class LoginController : Controller
    {

        private readonly LojaSimplesContext db;

        /// <summary>
        /// Cria uma nova instanca do controlador de dominio.
        /// </summary>
        /// <param name="context"></param>
        public LoginController(LojaSimplesContext context)
        {
            db = context;
        }

        /// <summary>
        /// Atentica o usuário na aplicação.
        /// </summary>
        /// <param name="credenciais">Email e senha capturados via JSON.</param>
        /// <returns></returns>
        [AllowAnonymous]
        [HttpPost]
        public async Task<IActionResult> Login([FromBody]CredenciaisLogin credenciais)
        {
            String cpf = "";
            if (credenciais != null && !String.IsNullOrWhiteSpace(credenciais.Email))
            {
                Cliente clienteCadastrado = db.Cliente.FirstOrDefault(cliente=>cliente.Email==credenciais.Email);
                if (clienteCadastrado != null && credenciais.Email == clienteCadastrado.Email && credenciais.Senha == clienteCadastrado.Senha)
                    cpf = clienteCadastrado.Cpf;
                else
                    return Forbid();

            }

            return Ok( cpf);
            
            
        }
    }
}
