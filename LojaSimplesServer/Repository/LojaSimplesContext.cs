﻿using LojaSimplesServer.Model;
using Microsoft.EntityFrameworkCore;

namespace LojaSimplesServer.Repository
{
    public partial class LojaSimplesContext : DbContext
    {
        public DbSet<Cliente> Cliente { get; set; }

        public LojaSimplesContext(DbContextOptions<LojaSimplesContext> options):base(options)
        {}
        
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {}
    }
}
